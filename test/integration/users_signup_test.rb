require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

	test "invalid signup information" do
		get signup_path
		assert_select "form[action=?]", "/signup"
		assert_no_difference 'User.count' do
			post users_path, params:{ 
							  user:{
							  		name: "",
							  		email:"cnu@invalid", 
							  		password: "foo",
							  		password_confirmation: "bar"
								}
							}
		end
		assert_template 'users/new'
		
		assert_select "li", text:"Name can't be blank"
		assert_select "li", text:"Email is invalid"
		assert_select "li", text:"Password confirmation doesn't match Password"
		assert_select "li", text:"Password is too short (minimum is 6 characters)"
=begin
#didn't work - check syntax 
		assert_select "div#error_explanation",
		"The form contains 1 error

			Name can't be blank"
			Email is invalid
			Password confirmation doesn't match Password
			Password is too short (minimum is 6 characters)"
=end
	end

	test "valid sign up information" do 
		get signup_path

		assert_difference 'User.count', 1 do
			post users_path, params:{
				user:{
					name: "babul",
					email: "babul@gmail.com",
					password: "123456",
					password_confirmation: "123456"
				}
			}
		end
		follow_redirect!
		assert_template 'users/show'  #good example of integration test since
				#it tests routes, show action and show template
		assert_not flash.empty?
		assert is_logged_in?
	end


end
