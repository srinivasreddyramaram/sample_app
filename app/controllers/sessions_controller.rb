class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by(email: params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
  		#login and show profile page
  		log_in(user) #creates a temporary cookie in browser
  		redirect_to user_url(user) #can also be shortened to 'redirect_to user'
  	else
  		#show errors and go to new page
  		flash.now[:danger] = "Invalid email/password combination"
  		render 'new'	
  	end  	
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
